FROM ubuntu:latest

# Update package lists and install curl
RUN apt-get update && apt-get install -y curl

# Set the working directory (optional)
WORKDIR /app

# Your additional configuration and commands here

# Command to run when the container starts (optional)
CMD ["bash"]